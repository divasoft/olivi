// swipebox
// --------------------------------------------------

if ($('.lightcase').length != 0) {
  $('.lightcase').lightcase({
    swipe: true,
    labels: {
      'sequenceInfo.of': '/'
    },
    onBeforeShow: {
	    quux: function() {
        $('body').find('.lightcase-icon-close').appendTo('#lightcase-case');
        $('body').find('.lightcase-icon-prev').appendTo('#lightcase-case');
        $('body').find('.lightcase-icon-next').appendTo('#lightcase-case');


        if ($('body').find('.lightcase-icon-close').find('.icon').length == 0) {
          $('body').find('.lightcase-icon-close').append('<span class="icon icon-close"></span>');
        }

        if ($('body').find('.lightcase-icon-prev').find('.icon').length == 0) {
          $('body').find('.lightcase-icon-prev').append('<span class="icon icon-arr_bold_simple_left"></span>');
        }

        if ($('body').find('.lightcase-icon-next').find('.icon').length == 0) {
          $('body').find('.lightcase-icon-next').append('<span class="icon icon-arr_bold_simple_right"></span>');
        }
      }
    }
  });
}

// swipebox end
// --------------------------------------------------

// selects
// --------------------------------------------------

selects2Init();
function selects2Init() {
	if ($('select').length != 0) {
		$('select:not(.default)').each(function() {
			var ths = $(this);
			var cls = ths.attr('class');

			ths.select2({
        placeholder: '',
				minimumResultsForSearch: Infinity
			});

			ths.siblings('.select2').addClass(cls);
			ths.siblings('.select2').find('b').addClass('icon icon-simple_arr_up');

      $('select').on('select2:open', function (evt) {
        $('.select2-container').addClass(cls + '_dropdown');
      });
      $('select').on('select2:select', function (evt) {
        $(this).trigger('change');
        if ($(this).closest('.form-element').hasClass('error_outer')) {
          $(this).closest('.form-element').removeClass('error_outer');
          $(this).closest('.form-element').addClass('valid_outer');
          $(this).siblings('span.error').remove();
        }
      });
		});
	}
}

// selects end
// --------------------------------------------------

// scrollbars
// --------------------------------------------------

$(function() {
	if ($('.customscroll').length != 0) {
		$(".customscroll").mCustomScrollbar({
			scrollbarPosition: 'outside'
		});
		$(window).on("load",function(){
			$(".customscroll").mCustomScrollbar('update');
		});
	}
	if ($('.customscroll_horisontal').length != 0) {
		$(".customscroll_horisontal").mCustomScrollbar({
			scrollbarPosition: 'outside',
			axis: 'x',
			advanced:{ autoExpandHorizontalScroll: true }
		});
		$(window).on("load",function(){
			$(".customscroll_horisontal").mCustomScrollbar('update');
		});
	}
});

// scrollbars end
// --------------------------------------------------

// masked input
// --------------------------------------------------

if ($('input[name="phone"]').length != 0) {
	$('input[name="phone"]').mask('+7 (999) 999-99-99');
}
if ($('input[name="tel"]').length != 0) {
	$('input[name="tel"]').mask('+7 (999) 999-99-99');
}

// masked input end
// --------------------------------------------------

// body attaching
// --------------------------------------------------

function bodyAttach() {
  $('html, body').css('overflow', 'hidden');
  if ($('html').hasClass('tablet') || $('html').hasClass('mobile')) {
    $('html').css('padding-right', 0);
    $('.header').css('padding-right', 0);
  } else {
    $('html').css('padding-right', 17);
    $('.header').css('padding-right', 17);
  }
}
function bodyDetach() {
  $('html, body').removeAttr('style');
  $('.header').css('padding-right', 0);
}

// body attaching end
// --------------------------------------------------

// fullpage sections
// --------------------------------------------------

function fpSectionUpdate() {
	if ($('.section-fullpage').length != 0) {
		$('.section-fullpage').each(function() {
			var ths = $(this);

			var win_height = $(window).outerHeight();

			ths.css('height', win_height);
		});
	}
}
fpSectionUpdate();
$(window).on('load resize', fpSectionUpdate);

// fullpage sections end
// --------------------------------------------------

// .icon-search-animated
// --------------------------------------------------

$('.icon-search-animated').click(function() {
  if ($(this).hasClass('active')) {
    $(this).removeClass('active');
  } else {
    $(this).addClass('active');
  }
});

// .icon-search-animated end
// --------------------------------------------------

// video custom splash
// --------------------------------------------------

$('.video__outer .play').click(function() {
	var $video = $(this).siblings('iframe'),
	src = $video.attr('src');
	$video.attr('src', src + '&autoplay=1');

	$video.siblings('.before').css('opacity', 0);
	$video.siblings('.play').css('opacity', 0);
	setTimeout(function() {
		$video.siblings('.before').remove();
		$video.siblings('.play').remove();
	}, 300);
});

// video custom splash end
// --------------------------------------------------

// on-screen
// --------------------------------------------------

function elementOnScreen() {
	if ($('.on-screen')) {
		$('.on-screen').each(function() {
			var ths = $(this);

			var scrl = $(window).scrollTop();
			var win_height = $(window).outerHeight();

			var ths_scrl = ths.offset().top;
			var ths_height = ths.outerHeight();

			if ((ths_scrl - scrl) > (-win_height/1.4) && (ths_scrl - scrl) < (win_height/1.4)) {
				setTimeout(function() {
					ths.addClass('on-screen-true');
					ths.removeClass('on-screen');
				}, 300);
			}
		});
	}
}
elementOnScreen();
$(window).on('scroll', elementOnScreen);

// on-screen end
// --------------------------------------------------

// modals
// --------------------------------------------------

$('.modal').on('show.bs.modal', function() {
  $('html').css('overflow', 'hidden');
});
$('.modal').on('hidden.bs.modal', function() {
  $('html').removeAttr('style');
});

// modals end
// --------------------------------------------------

// shave
// --------------------------------------------------

shaveRefresh();
$(window).on('load resize', shaveRefresh);

function shaveRefresh() {
	if ($('.shave').length != 0) {
		$('.shave').each(function() {
      var ths = $(this);
			var height = 100;

			if (ths.closest('p').hasClass('title') && ths.closest('.catalog__element').length != 0) height = 35;
      else if (ths.closest('p').hasClass('subtitle') && ths.closest('.catalog__element').length != 0) height = 20;

      var txt = ths.text();
      ths.attr('title', txt);

			ths.shave(height);
		});
	}
}

// shave end
// --------------------------------------------------

// accordion
// --------------------------------------------------

$('.accordion-tgl').click(function() {
  accordionLogic($(this));
});
if ($('.accordion-element').length != 0) {
  $('.accordion-element.opened').each(function() {
    var height = $(this).find('.accordion-inner').outerHeight();
    $(this).find('.accordion-body').css('max-height', height);
  });
}

function accordionLogic(ths) {
  var elem = ths.closest('.accordion-element');
  if (elem.hasClass('opened')) {
    elem.removeClass('opened');
    elem.find('.accordion-body').removeAttr('style');
  } else {
    elem.addClass('opened');
    var height = elem.find('.accordion-inner').outerHeight();
    elem.find('.accordion-body').css('max-height', height);
  }
}

// accordion end
// --------------------------------------------------

// forms
// --------------------------------------------------

// placeholders
placeholdersInit();
function placeholdersInit() {
	var placeholderChange = function(ths) {
		if (ths.attr('data-placeholder')) {
			var placeholder = ths.attr('data-placeholder');
			if (ths.siblings('.placeholder').length == 0) {
				ths.after('<span class="placeholder">' + placeholder + '</span>');
			}
		} else if (ths.attr('data-placeholder')) {
			var placeholder = ths.attr('placeholder');
			if (ths.siblings('.placeholder').length == 0) {
				ths.after('<span class="placeholder">' + placeholder + '</span>');
			}
		}
	}
	$('input, textarea, select').each(function() {
		var ths = $(this);
		if (ths.attr('type') != 'radio' && ths.attr('type') != 'checkbox' && ths.attr('type') != 'submit' && !ths.hasClass('number') && !ths.hasClass('no-placeholder')) {
			if (!ths.hasClass('simple')) {
				if (!ths.attr('data-placeholder')) {
					ths.attr('data-placeholder', ths.attr('placeholder'));
				}

				if (ths.val() != '') {
					ths.closest('.form-element').addClass('notempty');
				}
				placeholderChange(ths);
			}
		}
	});

	$('.placeholder').click(function() {
		$(this).closest('.form-element').addClass('active');
		if ($(this).siblings('input').length != 0) {
			$(this).siblings('input').focus();
		}
		if ($(this).siblings('textarea').length != 0) {
			$(this).siblings('textarea').focus();
		}
	});

	$('input:not([type="submit"]), textarea').focus(function() {
		$(this).closest('.form-element').addClass('active');
	});
	$('input:not([type="submit"]), textarea').on('keydown keyup keypress', function() {
		if ($(this).val() != '') {
			$(this).closest('.form-element').addClass('notempty');
		} else {
			$(this).closest('.form-element').removeClass('notempty');
		}
	});
	$('input:not([type="submit"]), textarea').blur(function() {
		if ($(this).val() == '') {
			$(this).closest('.form-element').removeClass('active');
			$(this).closest('.form-element').removeClass('notempty');
		} else {
			$(this).closest('.form-element').addClass('notempty');
		}
	});
	$('select').change(function() {
		$(this).closest('.form-element').addClass('notempty');
	});
}

// radio & checkboxes
radioCheckboxes();
function radioCheckboxes() {
	for (var i = 0; i < $('.checkbox').length; i++) {
		if (!$('.checkbox').eq(i).find('input[type="checkbox"]').is('[id]')) {
			$('.checkbox').eq(i).find('input[type="checkbox"]').attr('id', 'checkbox_' + i);
			$('.checkbox').eq(i).find('label').attr('for', 'checkbox_' + i);
		}
		if ($('.checkbox').eq(i).find('.inner').length == 0) {
			$('.checkbox').eq(i).find('label').wrapInner('<span class="inner"></span>');
			$('.checkbox').eq(i).find('label').append('<span class="after icon icon-check"></span>');
		}

	}
	for (var j = 0; j < $('.radio').length; j++) {
		if (!$('.radio').eq(j).find('input[type="radio"]').is('[id]')) {
			$('.radio').eq(j).find('input[type="radio"]').attr('id', 'radio_' + j);
			$('.radio').eq(j).find('label').attr('for', 'radio_' + j);
		}
		if ($('.radio').eq(i).find('.inner').length == 0) {
			$('.radio').eq(i).find('label').wrapInner('<span class="inner"></span>');
		}
	}
}

// forms end
// --------------------------------------------------


// callback__form
// --------------------------------------------------

if ($('.modal__catalog-subscribe__form').length != 0) {
	$('.modal__catalog-subscribe__form').validate({
		rules: {
			email: {required: true, email: true, truemail: true},
			catalog_subscribe__conf: {required: true}
		},
    messages: {
      email: {
        required: "Данное поле не заполнено",
        email: "Неверный формат данных",
        truemail: "Неверный формат данных"
      },
			catalog_subscribe__conf: {
				required: "Вы не выбрали данный флажок",
			}
    },

		errorElement: "span",
		errorPlacement: function(error, element) {alert()
			element.closest('.form-element').removeClass('valid_outer');
			element.closest('.form-element').addClass('error_outer');

			error.prependTo( element.parent("div") );
		},
	  success: function(label) {
			label.closest('.form-element').addClass('valid_outer');
			label.closest('.form-element').removeClass('error_outer');

	    label.addClass("valid").html('<span class="icon icon-check"></span>')
	  },
	});
}

// callback__form end
// --------------------------------------------------

// .modal__sign-in__form
// --------------------------------------------------

if ($('.modal__sign-in__form').length != 0) {
	$('.modal__sign-in__form').validate({
		rules: {
			email: {required: true, email: true, truemail: true},
			password: {required: true, minlength: 6}
		},
    messages: {
      email: {
        required: "Данное поле не заполнено",
	      email: "Неверный формат данных",
        truemail: "Неверный формат данных"
      },
			password: {
				required: "Данное поле не заполнено",
				minlength: "Введите не менее 6 символов"
			}
    },

		errorElement: "span",
		errorPlacement: function(error, element) {
			element.closest('.form-element').removeClass('valid_outer');
			element.closest('.form-element').addClass('error_outer');
			error.prependTo( element.parent("div") );
		},
	  success: function(label) {
			label.closest('.form-element').addClass('valid_outer');
			label.closest('.form-element').removeClass('error_outer');
	    label.addClass("valid").html('<span class="icon icon-check"></span>')
	  },
	});
}

function signInModalOpen() {
	$('.modal').modal('hide');

	setTimeout(function() {
		$('#modal__sign-in').modal('show');
	}, 300);
}

$('.to-new-password').click(newPasswordModalOpen);

// .modal__sign-in__form end
// --------------------------------------------------

// .modal__sign-up__form
// --------------------------------------------------

if ($('.modal__sign-up__form').length != 0) {
	$('.to-sign-up__btn').click(function() {
		if ($(this).closest('.modal').attr('id') == 'modal__sign-in') {
			// если переход с попапа авторизации
			var email = $(this).closest('.to-sign-up').find('input[name="email"]').val();
			var email_reg = /^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$/i.test(email);

			var confidencial = $(this).closest('.to-sign-up').find('#signup_confidencial');

			$(this).closest('.to-sign-up').find('input').removeClass('error');
			$(this).closest('.to-sign-up').find('input').siblings('span.error').remove();

			if (email_reg == true && confidencial.prop('checked') == true) {
				$('.modal').modal('hide');

				$('.modal__sign-up__form').find('input.email').val(email);

				setTimeout(function() {
					$('#modal__sign-up').modal('show');
				}, 300);
			} else {
				$('.to-sign-up').find('span.error').remove();

				if (email_reg != true) {
					var error_txt = 'Неверный формат данных';
					if (email == '') {
						error_txt = 'Данное поле не заполнено';
					}

					$('.to-sign-up input[name="email"]').addClass('error');
					$('.to-sign-up input[name="email"]').after('<span class="error">'+error_txt+'</span>');
				} else {
					$('.to-sign-up input[name="email"]').removeClass('error');
				}

				if (confidencial.prop('checked') != true) {
					confidencial.addClass('error');
					confidencial.closest('.form-element').append('<span class="error">Вы не выбрали данный флажок</span>');
				} else {
					confidencial.removeClass('error');
				}
			}
		} else if ($(this).closest('.modal').attr('id') == 'modal__new-password__first') {
			// если переход с попапа восстановления пароля
			var email = $(this).closest('.modal').find('.email').val();
			var email_reg = /^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$/i.test(email);

			$(this).closest('.modal').find('.email').removeClass('error');
			$(this).closest('.modal').find('.email').siblings('span.error').remove();

			if (email_reg == true) {
				$('.modal').modal('hide');

				$('.modal__sign-up__form').find('input.email').val(email);

				setTimeout(function() {
					$('#modal__sign-up').modal('show');
				}, 300);
			} else {
				var error_txt = 'Неверный формат данных';
				if (email == '') {
					error_txt = 'Данное поле не заполнено';
				}

				$(this).closest('.modal').find('.email').addClass('error');
				$(this).closest('.modal').find('.email').after('<span class="error">'+error_txt+'</span>');
			}
		}
	});

	$('.modal__sign-up__form').validate({
		rules: {
			name: {required: true, lettersonly: true},
			subscribe: {required: true},
			password: {required: true, minlength: 6},
			confirm_password: {required: true, minlength: 6, equalTo: "#modal__sign-up__form__password"},
			signup_confidencial_final: {required: true}
		},
    messages: {
			name: {
        required: "Данное поле не заполнено",
        lettersonly: "Неверный формат данных"
      },
      email: {
        required: "Данное поле не заполнено",
	      email: "Неверный формат данных",
        truemail: "Неверный формат данных"
      },
			subscribe: {
				required: "Вы не выбрали данный флажок",
			},
			password: {
				required: "Данное поле не заполнено",
				minlength: "Введите не менее 6 символов"
			},
			confirm_password: {
				required: "Данное поле не заполнено",
				minlength: "Введите не менее 6 символов",
				equalTo: "Пароли не совпадают"
			},
			signup_confidencial_final: {
				required: "Вы не выбрали данный флажок",
			}
    },

		errorElement: "span",
		errorPlacement: function(error, element) {
			element.closest('.form-element').removeClass('valid_outer');
			element.closest('.form-element').addClass('error_outer');
			error.prependTo( element.parent("div") );
		},
	  success: function(label) {
			label.closest('.form-element').addClass('valid_outer');
			label.closest('.form-element').removeClass('error_outer');
	    label.addClass("valid").html('<span class="icon icon-check"></span>')
	  },
	});

	$('#modal__sign-up .back-to-sign-in').click(signInModalOpen);
}

function signUpSuccess() {
	$('.modal').modal('hide');

	setTimeout(function() {
		$('#modal__sign-up__success').modal('show');
	}, 300);
}

// .modal__sign-up__form end
// --------------------------------------------------

// .modal__new-password__first__form
// --------------------------------------------------

function newPasswordModalOpen() {
	$('.modal').modal('hide');

	setTimeout(function() {
		$('#modal__new-password__first').modal('show');
	}, 300);
}

if ($('.modal__new-password__first__form').length != 0) {
	$('.modal__new-password__first__form').validate({
		rules: {
			email: {required: true, email: true, truemail: true}
		},
		messages: {
      email: {
        required: "Данное поле не заполнено",
	      email: "Неверный формат данных",
        truemail: "Неверный формат данных"
      }
		},

		errorElement: "span",
		errorPlacement: function(error, element) {
			element.closest('.form-element').removeClass('valid_outer');
			element.closest('.form-element').addClass('error_outer');
			error.prependTo( element.parent("div") );
		},
		success: function(label) {
			label.closest('.form-element').addClass('valid_outer');
			label.closest('.form-element').removeClass('error_outer');
			label.addClass("valid").html('<span class="icon icon-check"></span>')
		},
	});

	$('#modal__new-password__first .back').click(signInModalOpen);
}

function newPasswordFirstSuccess() {
	$('.modal').modal('hide');

	setTimeout(function() {
		$('#modal__new-password__first__success').modal('show');
	}, 300);
}

// .modal__new-password__first__form end
// --------------------------------------------------

// .modal__new-password__second__form
// --------------------------------------------------

function newPasswordSecondOpen() {
	$('.modal').modal('hide');

	setTimeout(function() {
		$('#modal__new-password__second').modal('show');
	}, 300);
}

if ($('.modal__new-password__second__form').length != 0) {
	$('.modal__new-password__second__form').validate({
		rules: {
			password: {required: true, minlength: 6},
			confirm_password: {required: true, minlength: 6, equalTo: "#modal__new-password__password"}
		},
		messages: {
			password: {
				required: "Данное поле не заполнено",
				minlength: "Введите не менее 6 символов"
			},
			confirm_password: {
				required: "Данное поле не заполнено",
				minlength: "Введите не менее 6 символов",
				equalTo: "Пароли не совпадают"
			}
		},

		errorElement: "span",
		errorPlacement: function(error, element) {
			element.closest('.form-element').removeClass('valid_outer');
			element.closest('.form-element').addClass('error_outer');
			error.prependTo( element.parent("div") );
		},
		success: function(label) {
			label.closest('.form-element').addClass('valid_outer');
			label.closest('.form-element').removeClass('error_outer');
			label.addClass("valid").html('<span class="icon icon-check"></span>')
		},
	});
}

function newPasswordSecondSuccess() {
	$('.modal').modal('hide');

	setTimeout(function() {
		$('#modal__new-password__second__success').modal('show');
	}, 300);
}

// .modal__new-password__second__form end
// --------------------------------------------------

// // modal-restore__form
// // --------------------------------------------------
//
// if ($('.modal-restore__form').length != 0) {
// 	$('.modal-restore__form').validate({
// 		rules: {
// 			email: {required: true, email: true, truemail: true}
// 		},
//     messages: {
//       email: {
//         required: "Данное поле не заполнено",
// 	      email: "Неверный формат данных",
//         truemail: "Неверный формат данных"
//       }
//     },
//
// 		errorElement: "span",
// 		errorPlacement: function(error, element) {
// 			error.prependTo( element.parent("div") );
// 		},
// 	});
// }
//
// // modal-restore__form end
// // --------------------------------------------------
//
// // modal-registration__form
// // --------------------------------------------------
//
// if ($('.modal-registration__form').length != 0) {
// 	$('.modal-registration__form').validate({
// 		rules: {
// 			name: {required: true, lettersonly: true},
// 			email: {required: true, email: true, truemail: true},
// 			password: {required: true, minlength: 6},
// 			confirm_password: {required: true, minlength: 6, equalTo: "#modal-registration__form_password"}
// 		},
//     messages: {
//       name: {
//         required: "Данное поле не заполнено",
//         lettersonly: "Неверный формат данных"
//       },
//       email: {
//         required: "Данное поле не заполнено",
// 	      email: "Неверный формат данных",
//         truemail: "Неверный формат данных"
//       },
// 			password: {
// 				required: "Данное поле не заполнено",
// 				minlength: "Введите не менее 6 символов"
// 			},
// 			confirm_password: {
// 				required: "Данное поле не заполнено",
// 				minlength: "Введите не менее 6 символов",
// 				equalTo: "Пароли не совпадают"
// 			}
//     },
//
// 		errorElement: "span",
// 		errorPlacement: function(error, element) {
// 			error.prependTo( element.parent("div") );
// 		},
// 	});
// }
//
// // modal-registration__form end
// // --------------------------------------------------
//
// // section__subscribe_form
// // --------------------------------------------------
//
// if ($('.section__subscribe_form').length != 0) {
// 	$('.section__subscribe_form').validate({
// 		rules: {
// 			email: {required: true, email: true, truemail: true}
// 		},
//     messages: {
//       email: {
//         required: "Данное поле не заполнено",
// 	      email: "Неверный формат данных",
//         truemail: "Неверный формат данных"
//       }
//     },
//
// 		errorElement: "span",
// 		errorPlacement: function(error, element) {
// 			error.prependTo( element.parent("div") );
// 		},
// 	});
// }
//
// // section__subscribe_form end
// // --------------------------------------------------
//
// // actions-list__subscribe_form
// // --------------------------------------------------
//
// if ($('.actions-list__subscribe_form').length != 0) {
// 	$('.actions-list__subscribe_form').validate({
// 		rules: {
// 			email: {required: true, email: true, truemail: true}
// 		},
//     messages: {
//       email: {
//         required: "Данное поле не заполнено",
// 	      email: "Неверный формат данных",
//         truemail: "Неверный формат данных"
//       }
//     },
//
// 		errorElement: "span",
// 		errorPlacement: function(error, element) {
// 			error.prependTo( element.parent("div") );
// 		},
// 	});
// }
//
// // actions-list__subscribe_form end
// // --------------------------------------------------
//
// // cart_discount_form
// // --------------------------------------------------
//
// if ($('.cart_discount_form').length != 0) {
// 	$('.cart_discount_form').validate({
// 		rules: {
// 			promocode: {required: true}
// 		},
//     messages: {
//       promocode: {
//         required: "Данное поле не заполнено"
//       }
//     },
//
// 		errorElement: "span",
// 		errorPlacement: function(error, element) {
// 			error.prependTo( element.parent("div") );
// 		},
// 	});
// }
//
// // cart_discount_form end
// // --------------------------------------------------
//
// // modal-hint__form
// // --------------------------------------------------
//
// if ($('.modal-hint__form').length != 0) {
// 	$('.modal-hint__form').validate({
// 		rules: {
// 			receiver_name: {required: true, lettersonly: true},
// 			sender_name: {required: true, lettersonly: true},
//       phone: {required: true},
// 			receiver_email: {required: true, email: true, truemail: true},
// 			sender_email: {required: true, email: true, truemail: true}
// 		},
//     messages: {
//       receiver_name: {
//         required: "Данное поле не заполнено",
//         lettersonly: "Неверный формат данных"
//       },
//       sender_name: {
//         required: "Данное поле не заполнено",
//         lettersonly: "Неверный формат данных"
//       },
//       phone: {
//         required: "Данное поле не заполнено"
//       },
//       receiver_email: {
//         required: "Данное поле не заполнено",
//         truemail: "Неверный формат данных"
//       },
//       sender_email: {
//         required: "Данное поле не заполнено",
//         truemail: "Неверный формат данных"
//       }
//     },
//
// 		errorElement: "span",
// 		errorPlacement: function(error, element) {
// 			error.prependTo( element.parent("div") );
// 		},
// 	});
// }
//
// // modal-hint__form end
// // --------------------------------------------------
//
// // modal-find_size
// // --------------------------------------------------
//
// if ($('.modal-find_size__form').length != 0) {
// 	$('.modal-find_size__form').validate({
// 		rules: {
// 			name: {required: true, lettersonly: true},
// 			email: {required: true, email: true, truemail: true}
// 		},
//     messages: {
//       name: {
//         required: "Данное поле не заполнено",
//         lettersonly: "Неверный формат данных"
//       },
//       email: {
//         required: "Данное поле не заполнено",
//         truemail: "Неверный формат данных"
//       }
//     },
//
// 		errorElement: "span",
// 		errorPlacement: function(error, element) {
// 			error.prependTo( element.parent("div") );
// 		},
// 	});
// }
//
// // modal-find_size end
// // --------------------------------------------------

// .userpage__data-form
// --------------------------------------------------

if ($('.userpage__data-form').length != 0) {
	$('.userpage__data-form').validate({
		rules: {
			name: {required: true, lettersonly: true},
			surname: {required: true, lettersonly: true},
			phone: {required: true},
			email: {required: true, email: true, truemail: true},
			date: {required: true},
			gender: {required: true, lettersonly: true}
		},
    messages: {
      name: {
        required: "Данное поле не заполнено",
        lettersonly: "Неверный формат данных"
      },
      surname: {
        required: "Данное поле не заполнено",
        lettersonly: "Неверный формат данных"
      },
			phone: {
				required: "Данное поле не заполнено"
			},
      email: {
        required: "Данное поле не заполнено",
        truemail: "Неверный формат данных"
      },
			date: {
				required: "Данное поле не заполнено"
			},
      gender: {
        required: "Данное поле не заполнено",
        lettersonly: "Неверный формат данных"
      }
    },

		errorElement: "span",
		errorPlacement: function(error, element) {
			element.closest('.form-element').removeClass('valid_outer');
			element.closest('.form-element').addClass('error_outer');
			error.prependTo( element.parent("div") );
		},
	  success: function(label) {
			label.closest('.form-element').addClass('valid_outer');
			label.closest('.form-element').removeClass('error_outer');
	    label.addClass("valid").html('<span class="icon icon-check"></span>')
	  },
	});
}

// .userpage__data-form end
// --------------------------------------------------

// .userpage__callback-form
// --------------------------------------------------

if ($('.userpage__callback-form').length != 0) {
	$('.userpage__callback-form').validate({
		rules: {
			theme: {required: true},
			message: {required: true}
		},
    messages: {
      theme: {
        required: "Выберите один из вариантов"
      },
      message: {
        required: "Данное поле не заполнено"
      }
    },

		errorElement: "span",
		errorPlacement: function(error, element) {
			element.closest('.form-element').removeClass('valid_outer');
			element.closest('.form-element').addClass('error_outer');
			error.prependTo( element.parent("div") );
		},
	  success: function(label) {
			label.closest('.form-element').addClass('valid_outer');
			label.closest('.form-element').removeClass('error_outer');
	    label.addClass("valid").html('<span class="icon icon-check"></span>')
	  },
	});
}

// .userpage__callback-form end
// --------------------------------------------------


// catalog menu add classes
// --------------------------------------------------

$('.menu-catalog > li').each(function() {
	if ($(this).find('ul.menu-catalog__submenu').length != 0) {
		$(this).addClass('submenu__outer');
	}
});

// catalog menu add classes end
// --------------------------------------------------

// header main logic
// --------------------------------------------------

if ($('html').hasClass('tablet') || $('html').hasClass('mobile')) {
	$('.outer').removeClass('header-top');
} else {
	if (!$('.outer').hasClass('header-static')) {
		if ($(window).outerWidth() > 750) {
			$(window).on('load scroll', headerChange);
		} else {
			$('.outer').removeClass('header-top');
		}

		$(window).on('resize', function() {
			if ($(window).outerWidth() <= 750) {
				$('.outer').removeClass('header-top');
			} else {
				headerChange();
			}
		});
	}
}

function headerChange() {
	var menu_catalog_height = $('.menu-catalog__outer').outerHeight();

	if ($(window).scrollTop() <= menu_catalog_height) {
		$('.outer').addClass('header-top');
	} else {
		$('.outer').removeClass('header-top');
	}
}

headerToTop();
$(window).on('resize load', headerToTop);
function headerToTop() {
  var header_height = $('.header').outerHeight();

	if ($(window).outerWidth() > 750 && $('html').hasClass('desktop')) {
		header_height = $('.header').outerHeight() + $('.menu-catalog__outer').outerHeight();
	}

  $('.outer').css('padding-top', header_height);
}

// header main logic end
// --------------------------------------------------

// header gender logic
// --------------------------------------------------

$('.header__gender').hover(function() {
	if (!$('.outer').hasClass('header-top')) {
		var gender_height = $('.header__gender__inner').outerHeight();

		$('.header__gender').css('min-height', gender_height);
	}
}, function() {
	$('.header__gender').removeAttr('style');
});

// header gender logic end
// --------------------------------------------------

// header catalog button logic
// --------------------------------------------------

$('.header__catalog-btn, .menu-catalog__outer').hover(function() {
	var catalog = $('.menu-catalog__outer');

	catalog.addClass('opened');
}, function() {
	var catalog = $('.menu-catalog__outer');

	catalog.removeClass('opened');
	catalog.addClass('closing');
	setTimeout(function() {
		catalog.removeClass('closing');
	}, 300)
});

// header catalog button logic end
// --------------------------------------------------

// header catalog menu logic
// --------------------------------------------------

var mainmeny_timer;

$('.menu-catalog > li').mouseenter(menuCatalogFocus);
$('.menu-catalog > li').mouseleave(menuCatalogBlur);

function menuCatalogFocus() {
	if ($(this).find('ul').length != 0 || $(this).hasClass('menu-catalog__submenu')) {
		clearTimeout(mainmeny_timer);

		if ($(window).outerWidth() > 750) {
			if ($('.outer').find('.gray_before').length == 0) {
				$('.outer').prepend('<div class="gray_before z40"></div>');
			}

			setTimeout(function() {
				$('.outer').find('.gray_before').addClass('opened');
			}, 10);
		}
	}
}
function menuCatalogBlur() {
	$('.outer').find('.gray_before').removeClass('opened');

	mainmeny_timer = setTimeout(function() {
		$('.outer').find('.gray_before').remove();
	}, 200);
}

// header catalog menu logic end
// --------------------------------------------------

// header search
// --------------------------------------------------

$('.header__search_tgl').click(function() {
	$('.header__search').css('display', 'block');
	$('.header__search__outer').addClass('opened');
	bodyAttach();

	setTimeout(function() {
		$('.header__search').addClass('opened');
	}, 10);
});

$('.header__search .close-btn, .header__search .before').click(function() {
	$('.header__search').removeClass('opened');
	$('.header__search__outer').removeClass('opened');
	$('.header__search__form')[0].reset();
	$('.header__search__form .form-element').removeClass('active notempty');

	setTimeout(function() {
		$('.header__search').removeAttr('style');
		bodyDetach();
	}, 300);
});

$('.header__search .clear-btn').click(function() {
	$(this).closest('form')[0].reset();
	$(this).closest('form').find('.form-element').removeClass('active notempty');
});

// header search end
// --------------------------------------------------

// header forming of side menu
// --------------------------------------------------

// copying elements
var gender_menu_copy = $('.header__gender').clone();
var callback_copy = $('.header__callback__outer').clone();
var favorites_copy = $('.header__favorites__outer').clone();
var userpages_copy = $('.header__userpage__outer').clone();
var catalog_menu_copy = $('.menu-catalog__outer').find('.menu-catalog').clone();
var phone_copy = $('.header__phone__outer').clone();
var footer_menu_copy = $('.footer__menu__outer').clone();

if ($('.side-menu__icons').find('.header__callback__outer').length == 0) {
	$('.side-menu__icons').append(callback_copy);
}
if ($('.side-menu__icons').find('.header__favorites__outer').length == 0) {
	$('.side-menu__icons').append(favorites_copy);
}
if ($('.side-menu__icons').find('.header__userpage__outer').length == 0) {
	$('.side-menu__icons').append(userpages_copy);
}

if ($('.side-menu__menu').find('.header__gender').length == 0) {
	$('.side-menu__inner').append(gender_menu_copy);
}

if ($('.side-menu__outer').find('.menu-catalog').length == 0) {
	$('.side-menu__inner').append(catalog_menu_copy);
}

if ($('.side-menu__inner').find('.footer__menu__outer').length == 0) {
	$('.side-menu__inner').append(footer_menu_copy);
}

if ($('.side-menu__container').find('.header__phone__outer').length == 0) {
	$('.side-menu__container').append(phone_copy);
}

// -------------------------------

$('.hamburger').click(function() {
	$('.side-menu__outer').css('display', 'block');
	bodyAttach();

	setTimeout(function() {
		$('.side-menu__outer').addClass('opened');
	}, 10);
});
$('.side-menu__outer .before').click(function() {
	$('.side-menu__outer').removeClass('opened');

	setTimeout(function() {
		$('.side-menu__outer').removeAttr('style');
		bodyDetach();
	}, 300);
});

// header forming of side menu end
// --------------------------------------------------

// header dropdown logic
// --------------------------------------------------

var head_dropdown_timer;
$('.icon__outer').mouseenter(function() {
	clearTimeout(head_dropdown_timer);
	$(this).addClass('opened');

	dropdownGrayBeforeCreate($(this));
}).mouseleave(function() {
	$(this).removeClass('opened');

	dropdownGrayBeforeRemove();
});

$('.header__dropdown').mouseenter(function() {
	clearTimeout(head_dropdown_timer);
	$(this).closest('.icon__outer').addClass('opened');

	dropdownGrayBeforeCreate($(this));
}).mouseleave(function() {
	$(this).closest('.icon__outer').removeClass('opened');

	dropdownGrayBeforeRemove();
});

$('.icon__outer > .icon').click(function() {
	$(this).closest('.icon__outer').addClass('opened');

	dropdownGrayBeforeCreate($(this));
});
$('.header__dropdown .close-btn, .gray_before').click(function() {
	$(this).closest('.icon__outer').removeClass('opened');

	dropdownGrayBeforeRemove();
});

function dropdownGrayBeforeCreate(ths) {
	if ($('.outer').find('.gray_before').length == 0 && ths.find('.header__dropdown').length != 0) {
		$('.header__icons, .side-menu__icons').prepend('<div class="gray_before"></div>');

		setTimeout(function() {
			$('.outer').find('.gray_before').addClass('opened');
		}, 100);
	}
}
function dropdownGrayBeforeRemove() {
	$('.outer').find('.gray_before').removeClass('opened');

	head_dropdown_timer = setTimeout(function() {
		$('.outer').find('.gray_before').remove();
	}, 200);
}

// header dropdown logic end
// --------------------------------------------------


// footer to down
// --------------------------------------------------

footerToDown();
$(window).on('resize load', footerToDown);

function footerToDown() {
  var header_height = $('.header').outerHeight();
  var footer_height = $('.footer').outerHeight();
  var window_height = $(window).outerHeight();

	if ($('.outer').hasClass('error_page_outer'))
	  $('.outer').css('height', window_height);
	else
		$('.outer').css('min-height', window_height);

  $('.outer').css('padding-bottom', footer_height);
}

// footer to down end
// --------------------------------------------------


// spinners
// --------------------------------------------------

spinnersInit();
function spinnersInit() {
  if ($('input[data-type="number"]').length != 0) {
    $('input[data-type="number"]').each(function() {
      var ths = $(this);
      ths.wrap('<div class="spinner"></div>');
      ths.after('<button class="spinner__plus spinner__btn icon icon-plus" data-dir="up"></button>');
      ths.after('<button class="spinner__minus spinner__btn icon icon-minus" data-dir="down"></button>');
    });
  }
}

var spinner_action;
$(".spinner__btn").on('click', function () {
  var ths = $(this),
		input = ths.siblings('input'),
		val = input.val(),
		min = input.attr('data-min'),
		max = input.attr('data-max'),
		newVal = 0;

	if (ths.attr('data-dir') == 'up') {
    if (parseInt(val) + 1 > max) {
      newVal = max
    } else {
      newVal = parseInt(val) + 1;
    }
	} else {
		if (val > min) {
			newVal = parseInt(val) - 1;
		} else {
			newVal = min;
		}
	}
	ths.siblings('input').val(newVal);
	ths.siblings('input').focus();
});
$(".spinner input").bind('input keyup', function() {
  if (this.value.match(/[^0-9.]/g)){
    this.value = this.value.replace(/[^0-9.]/g,'');
  }
});

// spinners end
// --------------------------------------------------


// sliders
// --------------------------------------------------

// slider-top
// --------------------------------------------------

if ($('.index__slider-top').length != 0) {
  $('.index__slider-top').slick({
    autoplay: true,
    autoplaySpeed: 6000,
    pauseOnHover: true,
    arrows: false,
    dots: true,
    appendDots: $('.index__slider-top__dots .container'),
    fade: true,
    cssEase: 'linear'
  });
}

// slider-top end
// --------------------------------------------------

// slider-in-trends
// --------------------------------------------------

if ($('.slider-in-trends').length != 0) {
  $('.slider-in-trends').each(function() {
    var ths = $(this);

    ths.find('.slide').each(function() {
      var slide = $(this);
      var new_slide;

      if (slide.find('.in-trends__element').hasClass('double-vertical')) {
        new_slide = slide.clone();
        new_slide.find('.element.elem-top').remove();
        new_slide.addClass('new');

        console.log(new_slide)

        slide.closest('.slide').after(new_slide)
      }
    });

    ths.slick({
      slidesToShow: 2,
      slidesToScroll: 1,
      infinite: false,
      arrows: true,
      dots: false,
      appendArrows: ths.closest('.section__slider__outer').find('.section__title'),
      prevArrow: '<span class="icon icon-arr_simple_left"></span>',
      nextArrow: '<span class="icon icon-arr_simple_right"></span>',
      responsive: [
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 1,
            variableWidth: true
          }
        }
      ]
    });
  });
}

// slider-in-trends end
// --------------------------------------------------

// slider-catalog
// --------------------------------------------------

if ($('.slider-catalog').length != 0) {
  $('.slider-catalog').each(function() {
    var ths = $(this);

    var slides_to_show = 3;
    if (ths.hasClass('lg4')) {
      slides_to_show = 4;
    }

    ths.slick({
      slidesToShow: slides_to_show,
      slidesToScroll: 1,
      infinite: false,
      arrows: true,
      dots: false,
      appendArrows: ths.closest('.section__slider__outer').find('.section__title'),
      prevArrow: '<span class="icon icon-arr_simple_left"></span>',
      nextArrow: '<span class="icon icon-arr_simple_right"></span>',
      afterChange: checkCatalogSlidesNumber(ths),
      responsive: [
        {
          breakpoint: 1200,
          settings: {
            slidesToShow: 3
          }
        }, {
          breakpoint: 768,
          settings: {
            slidesToShow: 2,
            variableWidth: true
          }
        }, {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            variableWidth: true
          }
        }
      ]
    });
    ths.on('afterChange', function() {
      checkCatalogSlidesNumber(ths)
    });

    var slider_numbers =
    '<div class="section__slider__numbers">' +
      '<span class="curr"></span>/' +
      '<span class="all"></span>' +
      '<div class="load">' +
        '<div class="load__inner"></div>' +
      '</div>' +
    '</div>';

    ths.closest('.section__slider__outer').find('.section__title').append(slider_numbers);

    checkCatalogSlidesNumber(ths);
  });
}

function checkCatalogSlidesNumber(slider) {
  var active_number = slider.find('.catalog__element.slick-active').last().index() + 1;
  var all_number = slider.find('.catalog__element').length;
  var percents = (active_number * 100) / all_number;

  var active_tag = slider.closest('.section__slider__outer').find('.section__slider__numbers .curr');
  var all_tag = slider.closest('.section__slider__outer').find('.section__slider__numbers .all');
  var load_tag = slider.closest('.section__slider__outer').find('.load__inner');

  active_tag.text(active_number);
  all_tag.text(all_number);
  load_tag.css('min-width', percents + '%');
}

// slider-catalog end
// --------------------------------------------------

// slider-catalog__element
// --------------------------------------------------

if ($('.slider-catalog__img').length != 0) {
  $('.slider-catalog__img').each(function() {
    var ths = $(this);

    ths.slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      infinite: false,
      arrows: false,
      dots: true,
      draggable: false,
      swipe: false,
      infinite: true,
      autoplaySpeed: 1000
    });

    ths.mouseenter(function() {
      ths.slick('slickPlay');
    });
    ths.mouseleave(function() {
      ths.slick('slickPause');
    });
  });
}

// slider-catalog__element end
// --------------------------------------------------

// slider-facts
// --------------------------------------------------

if ($('.slider-facts').length != 0) {
  $('.slider-facts').each(function() {
    var ths = $(this);

    ths.slick({
      slidesToShow: 3,
      slidesToScroll: 1,
      infinite: false,
      arrows: false,
      dots: false,
      responsive: [
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 2,
            variableWidth: true
          }
        }, {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            variableWidth: true
          }
        }
      ]
    });
  });
}

// slider-facts end
// --------------------------------------------------

// slider-brands
// --------------------------------------------------

if ($('.slider-brands').length != 0) {
  $('.slider-brands').each(function() {
    var ths = $(this);

    ths.slick({
      slidesToShow: 3,
      slidesToScroll: 1,
      infinite: false,
      arrows: false,
      dots: false,
      autoplay: true,
      autoplaySpeed: 2000,
      pauseOnHover: true,
      infinite: true,
      responsive: [
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 3,
            dots: true
          }
        }, {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            dots: true
          }
        }
      ]
    });
  });
}

// slider-brands end
// --------------------------------------------------

// slider-news
// --------------------------------------------------

if ($('.slider-news').length != 0) {
  $('.slider-news').each(function() {
    var ths = $(this);

    var slides_lg = 4
    if (ths.closest('.news__section').hasClass('with-subscribe')) {
      slides_lg = 2;
    }

    ths.slick({
      slidesToShow: slides_lg,
      slidesToScroll: 1,
      infinite: false,
      arrows: true,
      dots: false,
      appendArrows: ths.closest('.section__slider__outer').find('.section__title'),
      prevArrow: '<span class="icon icon-arr_simple_left"></span>',
      nextArrow: '<span class="icon icon-arr_simple_right"></span>',
      afterChange: checkNewsSlidesNumber(ths),
      responsive: [
        {
          breakpoint: 992,
          settings: {
            slidesToShow: 1
          }
        }, {
          breakpoint: 768,
          settings: {
            slidesToShow: 2
          }
        }, {
          breakpoint: 480,
          settings: {
            slidesToShow: 1
          }
        }
      ]
    });
    ths.on('afterChange', function() {
      checkNewsSlidesNumber(ths)
    });

    var slider_numbers =
    '<div class="section__slider__numbers">' +
      '<span class="curr"></span>/' +
      '<span class="all"></span>' +
      '<div class="load">' +
        '<div class="load__inner"></div>' +
      '</div>' +
    '</div>';

    ths.closest('.section__slider__outer').find('.section__title').append(slider_numbers);

    checkNewsSlidesNumber(ths);

    ths.on('edge', function(event, slick, direction) {
      console.log(direction)
      if (direction == 'left') {
        ths.slick('slickGoTo', 0);
      }
      if (direction == 'right') {
        var slidesToShow = ths.slick('slickGetOption', 'slidesToShow');
        var last_slide = parseInt(ths.find('.news__element:last-child').attr('data-slick-index'));

        if (slidesToShow > 1) {
          last_slide -= 1;
        }
        ths.slick('slickGoTo', last_slide);
      }
    });

    ths.closest('.slider-news__outer').siblings('.section__title').find('.icon-arr_simple_right').click(function() {
      if (ths.find('.news__element:last-child').hasClass('slick-active')) {

        ths.slick('slickGoTo', 0);
      }
    });

    ths.closest('.slider-news__outer').siblings('.section__title').find('.icon-arr_simple_left').click(function() {
      if (ths.find('.news__element:first-child').hasClass('slick-active')) {
        var slidesToShow = ths.slick('slickGetOption', 'slidesToShow');
        var last_slide = parseInt(ths.find('.news__element:last-child').attr('data-slick-index'));

        if (slidesToShow > 1) {
          last_slide -= 1;
        }
        ths.slick('slickGoTo', last_slide);
      }
    });
  });
}

function checkNewsSlidesNumber(slider) {
  var active_number = slider.find('.news__element.slick-active').last().index() + 1;
  var all_number = slider.find('.news__element:not(.slick-cloned)').length;
  var percents = (active_number * 100) / all_number;

  var active_tag = slider.closest('.section__slider__outer').find('.section__slider__numbers .curr');
  var all_tag = slider.closest('.section__slider__outer').find('.section__slider__numbers .all');
  var load_tag = slider.closest('.section__slider__outer').find('.load__inner');

  active_tag.text(active_number);
  all_tag.text(all_number);
  load_tag.css('min-width', percents + '%');

  console.log(active_number)
}

// slider-news end
// --------------------------------------------------


// catalog
// --------------------------------------------------

// catalog__element
// --------------------------------------------------

if ($('.favorites_btn_tgl').length != 0) {
  $('.favorites_btn_tgl').each(function() {
    var ths = $(this);

    if (ths.hasClass('active')) {
      ths.removeClass('icon-favorites');
      ths.addClass('icon-favorites_active');
    } else {
      ths.removeClass('icon-favorites_active');
      ths.addClass('icon-favorites');
    }
  });
}
$('.favorites_btn_tgl').click(function() {
  var ths = $(this);

  if (ths.hasClass('active')) {
    ths.removeClass('icon-favorites_active');
    ths.removeClass('active');
    ths.addClass('icon-favorites');
  } else {
    ths.removeClass('icon-favorites');
    ths.addClass('icon-favorites_active');
    ths.addClass('active');
  }
});

$('.catalog__element').mouseenter(function() {
  var ths = $(this);
  var hdn_inner_height = ths.find('.element__hidden__inner').outerHeight();

  ths.find('.element__hidden').css('max-height', hdn_inner_height);
});
$('.catalog__element').mouseleave(function() {
  var ths = $(this);
  var hdn_inner_height = ths.find('.element__hidden__inner').outerHeight();

  ths.find('.element__hidden').removeAttr('style');
});

// catalog__element end
// --------------------------------------------------


// filter
// --------------------------------------------------

$('.filter__element__head').click(function() {
  $('.filter__element').removeClass('closed');
});
$('.filter__element').hover(function() {
  $('.filter__element').removeClass('closed');
  var ths = $(this);
  var height = ths.outerHeight();
  var offset = ths.position().top;

  ths.find('.filter__element__content').css('top', height + offset + 10);
});

$('.filter__close').click(function() {
  $(this).closest('.filter__element').addClass('closed');
});

$('.filter__clear__local').click(function() {
  $(this).closest('.filter__element__content').find('input[type="text"]').val('');

  $(this).closest('.filter__element__content').find('input[type="checkbox"]').prop('checked', false);
  $(this).closest('.filter__element__content').find('input[type="radio"]').prop('checked', false);
});
$('.filter__clear').click(function() {
  $('.filter').find('input[type="text"]').val('');

  $('.filter').find('input[type="checkbox"]').prop('checked', false);
  $('.filter').find('input[type="radio"]').prop('checked', false);
});

// filter end
// --------------------------------------------------


// affix
// --------------------------------------------------

if ($('.affix__element').length != 0) {
  affixCheck();

  $(window).on('load resize', function() {
    if ($(window).outerWidth() > 750 && $('html').hasClass('desktop')) {
      $(".affix__element").mCustomScrollbar({
        scrollbarPosition: 'outside'
      });
    } else {
      $(".affix__element").mCustomScrollbar("destroy");
    }

    affixHeightCheck();
  });
}

function affixHeightCheck() {
  var cat_height = $('.affix__element .inner').outerHeight();
  var img_list_height = $('.affix__siblings').outerHeight();
  var affix_outer_height = $('.affix__outer').outerHeight();

  if (affix_outer_height < cat_height) {
    // $('.affix__outer').css('min-height', cat_height);

    $('.affix__element').addClass('static');
  } else {
    $('.affix__element').removeClass('static');
  }

  var height = $(window).outerHeight() - $('.header').outerHeight();

  // $('.affix__element').css('max-height', height);

  if (cat_height < img_list_height) {
    $('.affix__element').removeClass('noaffix');
  } else {
    $('.affix__element').addClass('noaffix');
  }
}
function affixCheck() {
  var x = 0;
  if ($(window).outerWidth() <= 750) {
    x = 60;
  }
  var cart_coeff = 0
  if ($('.affix__element').hasClass('cart__sidebar')) {
    cart_coeff = 50
  }

  $('.affix__element').affix({
    offset: {
      top: function () {
        var top_offset = $('.affix__outer .col-right').offset().top - $('.header').outerHeight();
        var top = $('.affix__outer .col-right').offset().top + $('.header').outerHeight();

        return (this.top = top_offset - (cart_coeff * 2))
      },
      bottom: function () {
        return (this.bottom = $('html').outerHeight() - $('.affix__end').offset().top + x)
      }
    }
  });
}

// affix end
// --------------------------------------------------

// cart
// --------------------------------------------------

if ($('.cart').length != 0) {
  $('.promocode__head').click(function() {
    $('.promocode').removeClass('noanimate');
    
    if ($('.promocode').hasClass('opened')) {
      promocodeClose();
    } else {
      promocodeOpen();
    }
  });
}
if ($('.promocode').length != 0 && $('.promocode').hasClass('opened')) {
  promocodeOpen();

  $(window).on('resize', promocodeCheckHeight);
}

function promocodeOpen() {
  var promocode_height = $('.promocode__inner').outerHeight();

  $('.promocode').addClass('opened');
  $('.promocode__body').css('max-height', promocode_height);
}
function promocodeCheckHeight() {
  var promocode_height = $('.promocode__inner').outerHeight();

  $('.promocode__body').css('max-height', promocode_height);
}
function promocodeClose() {
  $('.promocode').removeClass('opened');
  $('.promocode__body').removeAttr('style');
}

// cart end
// --------------------------------------------------

// order page
// --------------------------------------------------

$('.bonuses_radio__outer input[type="radio"]').on('change', function() {
  $('.bonuses_radio__outer .pl60').addClass('hdn')
  $(this).closest('.col-f').find('.pl60').removeClass('hdn');
});

// order page end
// --------------------------------------------------

// user page
// --------------------------------------------------

if ($('.userpage__orders').length != 0) {
  $(window).on('load resize', userpageOrdersHeight);
}

function userpageOrdersHeight() {
  var title_height = $('.userpage__orders').siblings('.userpage__title').outerHeight();
  var nav_height = $('.userpage__orders').siblings('.tabs-nav__outer').outerHeight();
  var sidebar_height = $('.userpage-sidebar').outerHeight();

  var height = sidebar_height - title_height - nav_height;

  $('.userpage__orders').css('min-height', height + 3);
}

// user page end
// --------------------------------------------------
